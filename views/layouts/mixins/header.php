<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

$navBarConfig = [
    'brandLabel' => 'Placet Group',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => ['class' => 'navbar-inverse navbar-fixed-top'],
];
?>

<?php NavBar::begin($navBarConfig) ?>
    <?php if (!Yii::$app->user->isGuest): ?>
        <?php
            echo Nav::widget([
                'options' => ['class' => 'nav navbar-nav'],
                'items' => [
                    [
                        'label' => Yii::t('navigation', 'profile'),
                        'url' => ['/profile'],
                        'active' => 'profile' === Yii::$app->controller->id,
                    ],
                    [
                        'label' => Yii::t('navigation', 'files'),
                        'url' => ['/file'],
                        'active' => 'file' === Yii::$app->controller->id,
                    ],
                ],
            ]);
    
            echo Nav::widget([
                'options' => ['class' => 'nav navbar-nav navbar-right'],
                'items' => [
                    [
                        'label' => Yii::$app->user->identity->getFullName(), 
                        'items' => [
                            [
                                'label' => Yii::t('navigation', 'changePassword'),
                                'url' => ['/auth/change-password']
                            ],
                            [
                                'label' => Yii::t('navigation', 'signOut'),
                                'url' => ['/auth/sign-out']
                            ],

                        ],
                    ],
                ],
            ])
        ?>
    <?php endif ?>
<?php NavBar::end() ?>

<?php

use app\helpers\CommonHelper;
use app\modules\core\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $signUpForm \app\models\forms\SignUpForm */
/* @var $this \yii\web\View */

$this->title = Yii::t('pageTitle', 'signUp');

$activeFormConfig = CommonHelper::activeFormConfig('sign-up-form', ['label' => 'col-sm-3 asterisk']);
$datePickerConfig = CommonHelper::datePickerConfig();
?>

<div class="col-sm-2"></div>
<div class="col-sm-8">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="panel-body">
            <?php $activeForm = ActiveForm::begin($activeFormConfig) ?>
                <?= $activeForm->field($signUpForm, 'login') ?>
                <?= $activeForm->field($signUpForm, 'password')->passwordInput() ?>
                <?= $activeForm->field($signUpForm, 'repeat_password')->passwordInput() ?>
                <?= $activeForm->field($signUpForm, 'first_name') ?>
                <?= $activeForm->field($signUpForm, 'last_name') ?>
                <?= $activeForm->field($signUpForm, 'email') ?>
                <?= $activeForm->field($signUpForm, 'phone') ?>
                <?= $activeForm->field($signUpForm, 'birth_date')->widget(DatePicker::className(), $datePickerConfig) ?>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <?= Html::submitButton(Yii::t('button', 'signUp'), ['class' => 'btn btn-primary']) ?>&nbsp;
                        <?= Html::a(Yii::t('button', 'cancel'), Yii::$app->getHomeUrl(), ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<div class="col-sm-2"></div>

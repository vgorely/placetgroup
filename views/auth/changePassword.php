<?php

use app\helpers\CommonHelper;
use app\modules\core\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $changePasswordForm \app\models\forms\ChangePasswordForm */
/* @var $this \yii\web\View */

$this->title = Yii::t('pageTitle', 'changePassword');

$activeFormConfig = CommonHelper::activeFormConfig('change-password-form', ['label' => 'col-sm-4 asterisk', 'control' => 'col-sm-8']);
?>

<div class="col-sm-2"></div>
<div class="col-sm-8">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="panel-body">
            <?php $activeForm = ActiveForm::begin($activeFormConfig) ?>
                <?= $activeForm->field($changePasswordForm, 'old_password')->passwordInput() ?>
                <?= $activeForm->field($changePasswordForm, 'new_password')->passwordInput() ?>
                <?= $activeForm->field($changePasswordForm, 'repeat_new_password')->passwordInput() ?>
            
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <?= Html::submitButton(Yii::t('button', 'change'), ['class' => 'btn btn-primary']) ?>&nbsp;
                        <?= Html::a(Yii::t('button', 'cancel'), Yii::$app->getHomeUrl()) ?>
                    </div>
                </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<div class="col-sm-2"></div>

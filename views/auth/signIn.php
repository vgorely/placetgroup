<?php

use app\helpers\CommonHelper;
use app\modules\core\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $signInForm \app\models\forms\SignInForm */
/* @var $this \yii\web\View */

$this->title = Yii::t('pageTitle', 'signIn');

$activeFormConfig = CommonHelper::activeFormConfig('sign-in-form');
?>

<div class="col-sm-3"></div>
<div class="col-sm-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="panel-body">
            <?php $activeForm = ActiveForm::begin($activeFormConfig) ?>
                <?= $activeForm->field($signInForm, 'login') ?>
                <?= $activeForm->field($signInForm, 'password')->passwordInput() ?>
            
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <?= Html::submitButton(Yii::t('button', 'signIn'), ['class' => 'btn btn-primary']) ?>&nbsp;
                        or&nbsp; 
                        <?= Html::a(Yii::t('button', 'signUp'), Url::to(['/auth/sign-up'])) ?>
                    </div>
                </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<div class="col-sm-3"></div>

<?php

use app\helpers\CommonHelper;
use app\modules\core\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Html;

/* @var $fileForm \app\models\forms\FileForm */

$activeFormConfig = CommonHelper::activeFormConfig('file-form', ['label' => 'col-sm-2 asterisk', 'control' => 'col-sm-10'], false);
$activeFormConfig['options']['enctype'] = 'multipart/form-data';

$fileInputConfig = CommonHelper::fileInputConfig();
?>

<?php $activeForm = ActiveForm::begin($activeFormConfig) ?>
    <?= $activeForm->field($fileForm, 'file')->widget(FileInput::classname(), $fileInputConfig) ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('button', 'save'), ['class' => 'btn btn-primary']) ?>&nbsp;
            <?= Html::a(Yii::t('button', 'cancel'), 'cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>

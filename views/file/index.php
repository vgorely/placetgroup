<?php

use app\assets\FileAsset;
use app\helpers\CommonHelper;
use app\modules\core\grid\GridView;
use app\modules\core\helpers\FileHelper;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;

/* @var $fileDataProvider \yii\data\ActiveDataProvider */
/* @var $this \yii\web\View */
/* @var $user \app\models\User */

FileAsset::register($this);
$this->title = Yii::t('pageTitle', 'fileList');

$fileList = 'file-list';
$fileModal = 'file-modal';
$this->registerJs("var fileList = '$fileList'; fileModal = '$fileModal';", View::POS_END);
?>

<?= Modal::widget(CommonHelper::modalConfig($fileModal)) ?>

<h1><?= Html::encode($this->title) ?></h1>
<?php Pjax::begin(['options' => ['id' => $fileList]]) ?>
    <?php if (Yii::$app->user->can('uploadFile', ['user_id' => $user->user_id])): ?>
        <p class="text-right">
            <?= Html::beginTag('button', ['class' => 'btn btn-primary add-file', 'data-href' => Url::to(['/file/upload/' . $user->user_id])]) ?>
            <?= Html::tag('i', '', ['class' => 'glyphicon glyphicon-plus']) ?> <?= Yii::t('button', 'addFile') ?>
            <?= Html::endTag('button') ?>
        </p>
    <?php endif ?>
    <?php
        echo GridView::widget([
            'dataProvider' => $fileDataProvider,
            'columns' => [
                [
                    'attribute' => 'filename',
                    'format' => 'html',
                    'value' => function($file) { return FileHelper::pathinfo($file->path, PATHINFO_BASENAME); },
                ],
                [
                    'attribute' => 'created_date',
                    'format' => 'datetime',
                    'headerOptions' => ['width' => '200'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'headerOptions' => ['width' => '100'],
                    'template' => '{download} {delete}',
                    'buttons' => [
                        'download' => function ($url, $file) {
                            if (Yii::$app->user->can('downloadFile', ['fileId' => $file->file_id])) {
                                $url = Url::to(['/file/download/' . $file->file_id]);
                                return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', $url, [
                                    'title' => Yii::t('core', '.download'),
                                    'data-pjax' => 0,
                                ]);
                            }
                        },
                        'delete' => function ($url, $file) {
                            if (Yii::$app->user->can('deleteFile', ['fileId' => $file->file_id])) {
                                $url = Url::to(['/file/delete/' . $file->file_id]);
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'class' => 'delete-file',
                                    'title' => Yii::t('core', 'title.delete'),
                                    'data-pjax' => 0,
                                ]);
                            }
                        },
                    ],
                ],
            ],
        ])
    ?>
<?php Pjax::end() ?>

<?php

use app\helpers\CommonHelper;
use app\modules\core\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Html;

/* @var $profileForm \app\models\forms\ProfileForm */

$activeFormConfig = CommonHelper::activeFormConfig('profile-form', ['label' => 'col-sm-3 asterisk']);
$activeFormConfig['options']['data-success'] = 'afterProfileSave';

$datePickerConfig = CommonHelper::datePickerConfig();
?>

<?php $activeForm = ActiveForm::begin($activeFormConfig) ?>
    <?= $activeForm->field($profileForm, 'login') ?>
    <?= $activeForm->field($profileForm, 'first_name') ?>
    <?= $activeForm->field($profileForm, 'last_name') ?>
    <?= $activeForm->field($profileForm, 'email') ?>
    <?= $activeForm->field($profileForm, 'phone') ?>
    <?= $activeForm->field($profileForm, 'birth_date')->widget(DatePicker::className(), $datePickerConfig) ?>

    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
            <?= Html::submitButton(Yii::t('button', 'save'), ['class' => 'btn btn-primary']) ?>&nbsp;
            <?= Html::a(Yii::t('button', 'cancel'), 'cancel', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
<?php ActiveForm::end() ?>
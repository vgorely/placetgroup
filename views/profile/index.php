<?php

use app\assets\ProfileAsset;
use app\helpers\CommonHelper;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use yii\widgets\DetailView;

/* @var $this \yii\web\View */
/* @var $user \app\models\User */

ProfileAsset::register($this);
$this->title = Yii::t('pageTitle', 'profile');

$profile = 'profile';
$profileModal = 'profile-modal';
$this->registerJs("var profile = '$profile', profileModal = '$profileModal';", View::POS_END);
?>

<?= Modal::widget(CommonHelper::modalConfig($profileModal)) ?>

<h1><?= Html::encode($this->title) ?></h1>
<?php Pjax::begin(['options' => ['id' => $profile]]) ?>
    <?php if (Yii::$app->user->can('editProfile', ['userId' => $user->user_id])): ?>
        <p class="text-right">
            <?= Html::beginTag('button', ['class' => 'btn btn-primary edit-profile', 'data-href' => Url::to(['/profile/edit/' . $user->user_id])]) ?>
            <?= Html::tag('i', '', ['class' => 'glyphicon glyphicon-edit']) ?> <?= Yii::t('button', 'editProfile') ?>
            <?= Html::endTag('button') ?>
        </p>
    <?php endif ?>
    <?php
        echo DetailView::widget([
            'model' => $user,
            'options' => ['class' => 'table table-striped table-bordered'],
            'attributes' => [
                [
                    'attribute' => 'login',
                ],
                [
                    'attribute' => 'first_name',
                ],
                [
                    'attribute' => 'last_name',
                ],
                [
                    'attribute' => 'email',
                    'format' => 'html',
                    'value' => $user->email ? Html::mailto($user->email, $user->email) : '',
                ],
                [
                    'attribute' => 'phone',
                ],
                [
                    'attribute' => 'birth_date',
                    'format' => 'date',
                ],
            ],
        ])
    ?>
<?php Pjax::end() ?>

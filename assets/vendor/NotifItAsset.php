<?php

namespace app\assets\vendor;

/**
 * @see https://github.com/naoxink/notifIt
 */
class NotifItAsset extends \yii\web\AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'vendor/notifIt/notifIt.css',
    ];

    public $js = [
        'vendor/notifIt/notifIt.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}

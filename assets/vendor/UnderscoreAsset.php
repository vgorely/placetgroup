<?php

namespace app\assets\vendor;

/**
  * @see https://github.com/components/underscore
 */
class UnderscoreAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/underscore';
    
    public $js = [
        'underscore-min.js',
    ];
}

<?php

namespace app\assets\vendor;

/**
 * @see https://github.com/makeusabrew/bootbox
 */
class BootboxAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/bootbox';
    
    public $js = [
        'bootbox.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}

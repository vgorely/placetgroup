<?php

namespace app\assets;

class FileAsset extends \yii\web\AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/file.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}

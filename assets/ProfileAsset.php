<?php

namespace app\assets;

class ProfileAsset extends \yii\web\AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/profile.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}

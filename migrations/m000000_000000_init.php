<?php

use yii\db\Migration;

class m000000_000000_init extends Migration
{
    private function importStructure()
    {
        $this->createTable('file', [
            'file_id' => 'int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'user_id' => 'int(11) NOT NULL',
            'path' => 'varchar(255) NOT NULL',
            'active' => 'boolean DEFAULT true NOT NULL',
            'created_date' => 'datetime NOT NULL',
        ]);
        
        $this->createTable('user', [
            'user_id' => 'int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'login' => 'varchar(32) NOT NULL',
            'password' => 'varchar(128) NOT NULL',
            'first_name' => 'varchar(64) NOT NULL',
            'last_name' => 'varchar(64) NOT NULL',
            'email' => 'varchar(128) DEFAULT NULL',
            'phone' => 'varchar(32) DEFAULT NULL',
            'birth_date' => 'date',
            'created_date' => 'datetime NOT NULL',
        ]);
        $this->createIndex('uniq_user_login', 'user', 'login', true);
        $this->createIndex('uniq_user_email', 'user', 'email', true);

        $this->createTable('user_history', [
            'user_history_id' => 'int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'user_id' => 'int(11) NOT NULL',
            'before' => 'text NOT NULL',
            'after' => 'text NOT NULL',
            'created_date' => 'datetime NOT NULL',
        ]);

        $this->addForeignKey('fk_file_user', 'file', 'user_id', 'user', 'user_id');
        $this->addForeignKey('fk_user_history_user', 'user_history', 'user_id', 'user', 'user_id');
    }
    
    private function importData()
    {
        $this->insert('user', [
            'login' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('123456'),
            'first_name' => 'John',
            'last_name' => 'Doe',
            'created_date' => date('Y-m-d H:i:s'),
        ]);
    }

    public function safeUp()
    {
        $this->importStructure();
        $this->importData();
    }

    public function safeDown()
    {
        $tables = ['file', 'user_history', 'user'];
        foreach ($tables as $table) {
            $this->dropTable($table);
        }
    }
}

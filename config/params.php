<?php

Yii::setAlias('@uploads', dirname(__DIR__) . '/uploads');

return [
    'file.maxNumber' => 20,
    
    'format.db.date' => 'yyyy-MM-dd',
    'format.db.datetime' => 'yyyy-MM-dd HH:mm:ss',
    'format.js.date' => 'yyyy-mm-dd',
    
    'grid.pageSize' => 10,
];

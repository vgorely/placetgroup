<?php

$db = require(__DIR__ . '/db.php');
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'placetgroup',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['user'],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => $db,
        'errorHandler' => [
            'errorAction' => 'app/error',
        ],
        'formatter' => [
            'nullDisplay' => '',
            'dateFormat' => $params['format.db.date'],
            'datetimeFormat' => $params['format.db.datetime'],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'fileMap' => [
                        'button' => 'button.php',
                        'http' => 'http.php',
                        'message' => 'message.php',
                        'modalHeader' => 'modalHeader.php',
                        'navigation' => 'navigation.php',
                        'pageTitle' => 'pageTitle.php',
                        'tableHeader' => 'tableHeader.php',
                        'title' => 'title.php',
                        'validation' => 'validation.php',
                    ],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'request' => [
            'cookieValidationKey' => 'OaZjLUvHOdYDfN1HwP0XNhTXaHMi2yA_',
        ],
        'response' => [
            'class' => 'app\modules\core\web\Response',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:[\w-]+>/<id:\d+>' => '<controller>/index',
                '<controller:[\w-]+>/<action:[\w-]+>' => '<controller>/<action>',
                '<controller:[\w-]+>/<action:[\w-]+>/<id:\d+>' => '<controller>/<action>',
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/auth/sign-in'],
        ],
        'view' => [
            'class' => 'app\modules\core\web\View',
        ],
    ],
    'defaultRoute' => '/profile',
    'language' => 'en',
    'layout' => 'default',
    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'app\modules\core\gii\Module';
}

return $config;

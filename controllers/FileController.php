<?php

namespace app\controllers;

use Yii;
use app\models\forms\FileForm;
use app\models\search\FileSearch;
use app\models\File;
use app\models\User;
use app\modules\core\helpers\FileHelper;
use app\modules\core\web\Response;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class FileController extends \app\modules\core\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param int $userId
     * @return User
     * @throws NotFoundHttpException
     */
    private function getUser($userId)
    {
        $user = User::findOne(['user_id' => $userId]);
        if (!$user) {
            throw new NotFoundHttpException(Yii::t('http', 'notFound'));
        }
        return $user;
    }

    /**
     * @param int $fileId
     * @return File
     * @throws NotFoundHttpException
     */
    private function getFile($fileId)
    {
        $file = File::findOne(['file_id' => $fileId, 'active' => true]);
        if (!$file) {
            throw new NotFoundHttpException(Yii::t('http', 'notFound'));
        }
        return $file;
    }

    /**
     * @param int|null $id User ID
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionIndex($id = null)
    {
        if ($id === null) {
            $id = Yii::$app->user->id;
        }

        $user = $this->getUser($id);
        if (!Yii::$app->user->can('viewFile', ['userId' => $user->user_id])) {
            throw new ForbiddenHttpException(Yii::t('http', 'forbidden'));
        }
        
        $fileSearch = new FileSearch(['user' => $user]);
        $fileDataProvider = $fileSearch->search(Yii::$app->request->get());

        return $this->render('index', [
            'user' => $user,
            'fileDataProvider' => $fileDataProvider
        ]);
    }

    /**
     * @param int $id User ID
     * @return array|Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpload($id)
    {
        $user = $this->getUser($id);
        if (!Yii::$app->user->can('uploadFile', ['userId' => $user->user_id])) {
            throw new ForbiddenHttpException(Yii::t('http', 'forbidden'));
        }
        
        $fileForm = new FileForm(['user' => $user]);
        if ($fileForm->load(Yii::$app->request->post())) {
            if (!$fileForm->save()) {
                $errors = $fileForm->getFirstErrors();
                Yii::$app->session->setFlash('flash', ['message' => implode('<br>', $errors), 'type' => 'error']);
                return $this->redirect(['/file']);
            }
            
            Yii::$app->session->setFlash('flash', [ 'message' => Yii::t('message', 'fileUploaded'), 'type' => 'success']);
            return $this->redirect(['/file']);
        }

        return [    
            'header' => Yii::t('modalHeader', 'addFile'),
            'body' => $this->renderAjax('form', ['fileForm' => $fileForm]),
        ];
    }

    /**
     * @param int $id File ID
     * @return bool|string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDownload($id)
    {
        $file = $this->getFile($id);
        if (!Yii::$app->user->can('downloadFile', ['fileId' => $file->file_id])) {
            throw new ForbiddenHttpException(Yii::t('http', 'forbidden'));
        }

        Yii::$app->response->format = Response::FORMAT_FILE;
        Yii::$app->response->filename = FileHelper::pathInfo($file->path, PATHINFO_BASENAME);
        
        return $file->getAbsolutePath();
    }

    /**
     * @param int $id File ID
     * @return array
     * @throws ForbiddenHttpException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $file = $this->getFile($id);
        if (!Yii::$app->user->can('deleteFile', ['fileId' => $file->file_id])) {
            throw new ForbiddenHttpException(Yii::t('http', 'forbidden'));
        }

        $file->active = false;
        if (!$file->save()) {
            throw new HttpException(501, Yii::t('http', 'notImplemented'));
        }
        $file->deleteFromStorage();

        return ['flash' => ['message' => Yii::t('message', 'fileDeleted'), 'type' => 'success']];
    }
}
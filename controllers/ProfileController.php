<?php

namespace app\controllers;

use Yii;
use app\models\forms\ProfileForm;
use app\models\User;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class ProfileController extends \app\modules\core\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param int $userId
     * @return User
     * @throws NotFoundHttpException
     */
    private function getUser($userId)
    {
        $user = User::findOne(['user_id' => $userId]);
        if (!$user) {
            throw new NotFoundHttpException(Yii::t('http', 'notFound'));
        }
        return $user;
    }

    /**
     * @param int|null $id User ID
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionIndex($id = null)
    {
        if ($id === null) {
            $id = Yii::$app->user->id;
        }
        
        $user = $this->getUser($id);
        if (!Yii::$app->user->can('viewProfile', ['userId' => $user->user_id])) {
            throw new ForbiddenHttpException(Yii::t('http', 'forbidden'));
        }
        
        return $this->render('index', [
            'user' => $user,
        ]);
    }

    /**
     * @param int $id User ID
     * @return array
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionEdit($id)
    {
        $user = $this->getUser($id);
        if (!Yii::$app->user->can('editProfile', ['userId' => $user->user_id])) {
            throw new ForbiddenHttpException(Yii::t('http', 'forbidden'));
        }
        
        $profileForm = new ProfileForm(['user' => $user]);
        if ($profileForm->load(Yii::$app->request->post())) {
            if (!$profileForm->save()) {
                return ['errors' => $profileForm->getErrors()];
            }

            return [
                'success' => true,
                'flash' => ['message' => Yii::t('message', 'profileSaved'), 'type' => 'success']
            ];
        }

        return [
            'header' => Yii::t('modalHeader', 'editProfile'),
            'body' => $this->renderAjax('form', ['profileForm' => $profileForm]),
        ];
    }
}
<?php

namespace app\controllers;

class AppController extends \app\modules\core\web\Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}

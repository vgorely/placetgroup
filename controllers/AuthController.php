<?php

namespace app\controllers;

use Yii;
use app\models\forms\ChangePasswordForm;
use app\models\forms\SignInForm;
use app\models\forms\SignUpForm;
use app\modules\core\web\Response;
use yii\filters\AccessControl;

class AuthController extends \app\modules\core\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['change-password', 'sign-out'],
                'rules' => [
                    [
                        'actions' => ['change-password', 'sign-out'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * @return array|string|Response
     */
    public function actionSignUp()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $signUpForm = new SignUpForm();
        if ($signUpForm->load(Yii::$app->request->post())) {
            if (!$signUpForm->save()) {
                return ['errors' => $signUpForm->getErrors()];
            }
            
            return $this->redirect(['/auth/sign-in']);
        }

        return $this->render('signUp', ['signUpForm' => $signUpForm]);
    }

    /**
     * @return array|string|Response
     */
    public function actionSignIn()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $signInForm = new SignInForm();
        if ($signInForm->load(Yii::$app->request->post())) {
            if (!$signInForm->save()) {
                return ['errors' => $signInForm->getErrors()];
            }

            return $this->goHome();
        }
        
        return $this->render('signIn', ['signInForm' => $signInForm]);
    }

    /**
     * @return array|string
     */
    public function actionChangePassword()
    {
        $user = Yii::$app->user->identity;
        
        $changePasswordForm = new ChangePasswordForm(['user' => $user]);
        if ($changePasswordForm->load(Yii::$app->request->post())) {
            if (!$changePasswordForm->save()) {
                return ['errors' => $changePasswordForm->getErrors()];
            }

            Yii::$app->session->setFlash('flash', ['message' => Yii::t('message', 'passwordChanged'), 'type' => 'success']);
            return $this->goHome();
        }

        return $this->render('changePassword', ['changePasswordForm' => $changePasswordForm]);
    }

    /**
     * @return Response
     */
    public function actionSignOut()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}

<?php

use app\rbac\rules;

return [
    'viewProfile' => serialize(new rules\profile\View()),
    'editProfile' => serialize(new rules\profile\Edit()),

    'viewFile' => serialize(new rules\file\View()),
    'uploadFile' => serialize(new rules\file\Upload()),
    'downloadFile' => serialize(new rules\file\Download()),
    'deleteFile' => serialize(new rules\file\Delete()),
];

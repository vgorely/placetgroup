<?php

namespace app\rbac\rules\file;

use Yii;
use app\models\User;

class Upload extends \yii\rbac\Rule
{
    public function execute($user, $item, $params)
    {
        $loggedUserId = $user;

        $user = User::findOne($loggedUserId);
        if ($user && $loggedUserId == $user->user_id &&
            $user->getActiveFileCount() < Yii::$app->params['file.maxNumber']) {
            
            return true;
        }
        
        return false;
    }
}

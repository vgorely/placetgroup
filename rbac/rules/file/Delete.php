<?php

namespace app\rbac\rules\file;

use Yii;
use app\models\File;
use yii\helpers\ArrayHelper;

class Delete extends \yii\rbac\Rule
{
    public function execute($user, $item, $params)
    {
        $loggedUserId = $user;
        
        $file = File::findOne(ArrayHelper::getValue($params, 'fileId'));
        if ($file && $loggedUserId == $file->user_id) {
            return true;
        }
        
        return false;
    }
}

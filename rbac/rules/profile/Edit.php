<?php

namespace app\rbac\rules\profile;

use Yii;
use app\models\User;
use yii\helpers\ArrayHelper;

class Edit extends \yii\rbac\Rule
{
    public function execute($user, $item, $params)
    {
        $loggedUserId = $user;
        
        $user = User::findOne(ArrayHelper::getValue($params, 'userId'));
        if ($user && $loggedUserId == $user->user_id) {
            return true;
        }
        
        return false;
    }
}

<?php

use yii\rbac\Item;

return [
    // Roles
    'user' => [
        'type' => Item::TYPE_ROLE,
        'description' => 'User role',
        'children' => [
            'viewProfile',
            'editProfile',

            'viewFile',
            'uploadFile',
            'downloadFile',
            'deleteFile',
        ],
    ],
    // /Roles

    // Permissions
    'viewProfile' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Permission to view user profile',
        'ruleName' => 'viewProfile',
    ],
    'editProfile' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Permission to edit user profile',
        'ruleName' => 'editProfile',
    ],

    'viewFile' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Permission to view user file',
        'ruleName' => 'viewFile',
    ],
    'uploadFile' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Permission to upload user file',
        'ruleName' => 'uploadFile',
    ],
    'downloadFile' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Permission to download user file',
        'ruleName' => 'downloadFile',
    ],
    'deleteFile' => [
        'type' => Item::TYPE_PERMISSION,
        'description' => 'Permission to delete user file',
        'ruleName' => 'deleteFile',
    ],
    // /Permissions
];

<?php

namespace app\modules\core\gii;

use ReflectionClass;

class Module extends \yii\gii\Module
{
    public $generators = [
        'model' => ['class' => 'app\modules\core\gii\generators\model\Generator']
    ];

    public function init()
    {
        $class = new ReflectionClass($this);
        $this->setBasePath(dirname($class->getParentClass()->getFileName()));
    }
}

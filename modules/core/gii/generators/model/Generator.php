<?php

namespace app\modules\core\gii\generators\model;

use ReflectionClass;
use Yii;

class Generator extends \yii\gii\generators\model\Generator
{
    public $ns = 'app\models\gii';
    public $baseClass = 'app\modules\core\db\ActiveRecord';
    
    public function defaultTemplate()
    {
        $class = new ReflectionClass($this);

        return dirname($class->getParentClass()->getFileName()) . '/default';
    }

    public function formView()
    {
        $class = new ReflectionClass($this);

        return dirname($class->getParentClass()->getFileName()) . '/form.php';
    }

    protected function generateRelations()
    {
        $relations = parent::generateRelations();
        
        foreach ($relations as &$relation) {
            foreach ($relation as &$item) {
                $item[0] = str_replace($item[1]  . '::', '\app\models\\' . $item[1] . '::', $item[0]);
            }
            ksort($relation);
        }
        ksort($relations);
        
        return $relations;
    }
}

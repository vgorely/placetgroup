<?php

namespace app\modules\core\web;

use Yii;

class Controller extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        
        if (is_array($result)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        
        return $result;
    }

}
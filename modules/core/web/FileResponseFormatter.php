<?php

namespace app\modules\core\web;

class FileResponseFormatter extends \yii\base\Component implements \yii\web\ResponseFormatterInterface
{
    /**
     * @inheritdoc
     */
    public function format($response)
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $response->filename);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($response->data));
        readfile($response->data);
        die();
    }
}

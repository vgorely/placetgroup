<?php

namespace app\modules\core\web;

class Response extends \yii\web\Response
{
    const FORMAT_FILE = 'file';
    
    public $filename = 'Unnamed';

    protected function defaultFormatters()
    {
        return array_merge(parent::defaultFormatters(), [
            self::FORMAT_FILE => 'app\modules\core\web\FileResponseFormatter',
        ]);
    }
}

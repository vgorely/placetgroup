<?php

namespace app\modules\core\web;

use Yii;
use yii\helpers\ArrayHelper;

class View extends \yii\web\View
{
    /**
     * @inheritdoc
     */
    public function render($view, $params = [], $context = null)
    {
        $result = parent::render($view, $params, $context);
        
        if (Yii::$app->session->hasFlash('flash')) {
            $flash = Yii::$app->session->getFlash('flash', null, true);
            $message = addslashes(ArrayHelper::getValue($flash, 'message'));
            $type = ArrayHelper::getValue($flash, 'type');
            $this->registerJs("app.flash('$message', '$type')");
        }
        
        return $result;
    }
}

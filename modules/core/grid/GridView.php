<?php

namespace app\modules\core\grid;

class GridView extends \yii\grid\GridView
{
    /**
     * @inheritdoc
     */
    public $layout = '
        <div class="grid-header clearfix">
            <div class="pull-left">{summary}</div>
        </div>
        {items}
        <div class="grid-footer clearfix">
            <div class="pull-right">{pager}</div>
        </div>';
}

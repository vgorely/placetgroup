<?php

namespace app\modules\core\helpers;

class FileHelper extends \yii\helpers\FileHelper
{
    /**
     * The distinctions from this method and native realization:
     *  - 'dirname' it is all characters from the to the first '/' character
     *  - 'pathinfo' does not work properly with UTF-8 characters (ex. cyrillic)
     * 
     * @param $path
     * @param int|null $options
     * @return array|string
     */
    public static function pathInfo($path, $options = null)
    {
        $pathInfo = array_fill_keys(['dirname', 'basename', 'filename', 'extension'], '');
        
        $pos = strrpos($path, '/');
        if ($pos === false) {
            $pathInfo['dirname'] = '';
            $pathInfo['basename'] = $path;
        } else {
            $pathInfo['dirname'] = substr($path, 0, $pos);
            $pathInfo['basename'] = substr($path, $pos + 1);
        }

        $pos = strrpos($pathInfo['basename'], '.');
        if ($pos === false) {
            $pathInfo['filename'] = $pathInfo['basename'];
            $pathInfo['extension'] = '';
        } else {
            $pathInfo['filename'] = substr($pathInfo['basename'], 0, $pos);
            $pathInfo['extension'] = substr($pathInfo['basename'], $pos + 1);
        }
        
        switch ($options) {
            case PATHINFO_DIRNAME:
                return $pathInfo['dirname'];
                break;
            case PATHINFO_BASENAME:
                return $pathInfo['basename'];
                break;
            case PATHINFO_FILENAME:
                return $pathInfo['filename'];
                break;
            case PATHINFO_EXTENSION:
                return $pathInfo['extension'];
                break;
            default:
                return $pathInfo;
        }
    }
    
    /**
     * @param string $path
     * @return string mixed
     */
    public static function incrementFilename($path)
    {
        $pathInfo = self::pathInfo($path);
        
        $counter = 1;
        if (preg_match('/^(.+)(-\d+)$/', $pathInfo['filename'], $matches)) {
            $pathInfo['filename'] = $matches[1];
            $counter = ltrim($matches[2], '-');
            $counter++;
        }
        $pathInfo['filename'] .= '-' . $counter;

        $path  = $pathInfo['dirname'] ? $pathInfo['dirname'] . '/' : '';
        $path .= $pathInfo['filename'];
        $path .= $pathInfo['extension'] ? '.' . $pathInfo['extension']  : '';
        return $path;
    }
}

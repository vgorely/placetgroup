<?php

namespace app\modules\core\widgets;

class ActiveForm extends \yii\widgets\ActiveForm
{
    public $events = [];

    public function run()
    {
        parent::run();
    
        $id = $this->options['id'];
        $view = $this->getView();
        if ($this->events) {
            foreach ($this->events as $event) {
                $view->registerJs("jQuery('#$id').on('$event', function() { return $event($('#$id')); });");
            }
        }
    }
} 
REQUIREMENTS
------------
  - PHP 5.4.*
  - MySQL 5.5.*
  - Composer http://getcomposer.org/
  - Git http://git-scm.com/


INSTALLATION
------------
  - Document root should refer to the **web** directory
  - Create a database and modify the file **config/db.php**
  - Run the commands to download application dependencies
```
php composer.phar global require "fxp/composer-asset-plugin:1.0.0-beta4"
php composer.phar install
```
  - Run the command to check application requirements
```
php requirements.php
```
  - Run the command to initialize database scheme
```
php yii migrate
```
  - Make sure that following directories are writable
~~~
uploads/
runtime/
web/assets/
~~~

<?php

return [
    'lettersOnly' => 'Only latin letters are allowed.',
    'numbersOnly' => 'Only numbers are allowed.',
];

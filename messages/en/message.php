<?php

return [
    'fileDeleted' => 'File has been deleted.',
    'fileUploaded' => 'File has been uploaded.',
    'historyNotSaved' => "User history hasn't been saved.",
    'passwordChanged' => 'Password has been changed.',
    'profileSaved' => 'Profile has been saved.',
    'userNotSaved' => "User hasn't been saved.",
];

<?php

return [
    'changePassword' => 'Change Password',
    'files' => 'Files',
    'profile' => 'Profile',
    'signOut' => 'Sign Out',
];

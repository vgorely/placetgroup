<?php

return [
    'forbidden' => "You don't have permission to perform this action.",
    'notFound' => 'The requested resource (or one of its dependencies) is not available.',
    'notImplemented' => 'Please contact the site administrator if the problem persists.',
];

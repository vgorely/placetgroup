<?php

return [
    'addFile' => 'Add File',
    'browse' => 'Browse',
    'cancel' => 'Cancel',
    'change' => 'Change',
    'editProfile' => 'Edit Profile',
    'remove' => 'Remove',
    'save' => 'Save',
    'signIn' => 'Sign In',
    'signUp' => 'Sign Up',
];

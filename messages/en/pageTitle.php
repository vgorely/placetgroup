<?php

return [
    'changePassword' => 'Change Password',
    'fileList' => 'Files',
    'profile' => 'Profile',
    'signIn' => 'Sign In',
    'signUp' => 'Sign Up',
];

<?php

namespace app\helpers;

use Yii;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CommonHelper
{
    /**
     * @param string $id
     * @return array
     */
    public static function modalConfig($id)
    {
        return [
            'id' => $id,
            'options' => ['data-backdrop' => 'static'],
            'header' => Html::tag('span'),
            'closeButton' => ['tag' => 'a'],
        ];
    }

    /**
     * @param string $id
     * @param array $classes
     * @param bool $beforeSubmit
     * @return array
     */
    public static function activeFormConfig($id, $classes = [], $beforeSubmit = true)
    {
        $classes = ArrayHelper::merge([
            'form' => ['form-horizontal'],
            'label' => 'col-sm-3',
            'control' => 'col-sm-9',
        ], $classes);

        $config = [
            'id' => $id,
            'enableClientValidation' => true,
            'options' => ['class' => implode(' ', $classes['form'])],
            'fieldConfig' => [
                'labelOptions' => ['class' => 'control-label ' . $classes['label']],
                'template' => '{label}<div class="' . $classes['control'] . '">{input}{error}</div>',
            ],
        ];
        if ($beforeSubmit) {
            $config['events']['beforeSubmit'] = 'beforeSubmit';
        }
        return $config;
    }
    
    /**
     * @return array
     */
    public static function datePickerConfig()
    {
        return [
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pluginOptions' => ['format' => Yii::$app->params['format.js.date'], 'autoclose' => true],
        ];
    }

    /**
     * @return array
     */
    public static function fileInputConfig()
    {
        return [
            'pluginOptions' => [
                'showPreview' => false,
                'showUpload' => false,
                'browseLabel' => Yii::t('button', 'browse') . '&hellip;',
                'removeLabel' => Yii::t('button', 'remove'),
            ]
        ];
    }
}

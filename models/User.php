<?php

namespace app\models;

use Yii;
use yii\db\Exception;
use yii\helpers\Json;

class User extends gii\User implements \yii\web\IdentityInterface
{
    // Interface methods
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return User::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return Yii::$app->security->generatePasswordHash($this->login);
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return Yii::$app->security->validatePassword($this->login, $authKey);
    }
    // /Interface methods

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['created_date'], 'date', 'format' => Yii::$app->params['format.db.datetime']];
        return $rules;
    }

    /**
     * @param string $password
     * @throws \yii\base\Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
    
    /**
     * @param $password
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    /**
     * @return int
     */
    public function getActiveFileCount()
    {
        return File::find()->where(['user_id' => $this->user_id, 'active' => true])->count();
    }

    /**
     * Put in order the attributes in order not to add such kind of records in the 'user_history' table
     * ~~~
     * [
     *     'before' => {"email":null},
     *     'after' => {"email":""},
     * ]
     * ~~~
     */
    private function adjustAttributes()
    {
        $attributes = ['email', 'phone', 'birth_date'];
        foreach ($attributes as $attribute) {
            if (!$this->{$attribute}) {
                $this->{$attribute} = null;
            }    
        }
    }

    /**
     * @return bool
     * @throws Exception
     * @throws \Exception
     */
    public function extendedSave()
    {
        $this->adjustAttributes();
        
        $changedAttributes = $this->getDirtyAttributes();
        if (!$changedAttributes) {
            return true;
        }

        $before = [];
        $after = [];
        foreach ($changedAttributes as $name => $value) {
            if (!$this->isNewRecord) {
                $before[$name] = $this->getOldAttribute($name);    
            }
            $after[$name] = $this->getAttribute($name);
        }

        $transaction = static::getDb()->beginTransaction();
        try {
            if (!$this->save()) {
                throw new Exception(Yii::t('message', 'userNotSaved'));
            }

            $userHistory = new UserHistory([
                'user_id' => $this->user_id,
                'before' => Json::encode($before),
                'after' => Json::encode($after),
                'created_date' => date('Y-m-d H:i:s'),
            ]);
            if (!$userHistory->save()) {
                throw new Exception(Yii::t('message', 'historyNotSaved'));
            }
            
            $transaction->commit();
            
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
}

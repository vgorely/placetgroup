<?php

namespace app\models\forms;

use Yii;
use app\models\User;

class SignUpForm extends \app\modules\core\base\Model
{
    public $login;
    public $password;
    public $repeat_password;
    public $first_name;
    public $last_name;
    public $email;
    public $phone;
    public $birth_date;

    /* @var User */
    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'repeat_password', 'first_name', 'last_name'], 'required'],
            [['login'], 'string', 'min' => 3, 'max' => 32],
            [['login'], 'match', 'pattern' => '/^[a-z]*$/i', 'message' => Yii::t('validation', 'lettersOnly')],
            [['login'], 'unique', 'targetClass' => 'app\models\User'],
            [['password'], 'string', 'min' => 6],
            [['repeat_password'], 'compare', 'compareAttribute' => 'password', 'operator' => '=='],
            [['first_name', 'last_name'], 'string', 'min' => 2, 'max' => 64],
            [['first_name', 'last_name'], 'match', 'pattern' => '/^[a-z]*$/i', 'message' => Yii::t('validation', 'lettersOnly')],
            [['email'], 'string', 'max' => 128],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => 'app\models\User'],
            [['phone'], 'string', 'max' => 32],
            [['phone'], 'match', 'pattern' => '/^[0-9]*$/', 'message' => Yii::t('validation', 'numbersOnly')],
            [['birth_date'], 'string'],
            [['birth_date'], 'date', 'format' => Yii::$app->params['format.db.date']],
        ];
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        if (!$this->user) {
            $this->user = new User([
                'login' => $this->login,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'email' => $this->email,
                'phone' => $this->phone,
                'birth_date' => $this->birth_date,
                'created_date' => date('Y-m-d H:i:s'),
            ]);
            $this->user->setPassword($this->password);
        }
        return $this->user;
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        
        $user = $this->getUser();
        return $user->extendedSave();
    }
}

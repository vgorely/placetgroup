<?php

namespace app\models\forms;

use Yii;
use app\models\User;

class SignInForm extends \app\modules\core\base\Model
{
    public $login;
    public $password;

    /* @var User */
    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login', 'password'], 'filter', 'filter' => 'trim'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        if (!$this->user) {
            $this->user = User::findOne(['login' => $this->login]);
        }
        return $this->user;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function validatePassword()
    {
        $user = $this->getUser();
        if (!$user || !$user->validatePassword($this->password)) {
            $this->addError('password', 'Incorrect login or password.');
        }
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        
        $user = $this->getUser();
        return Yii::$app->user->login($user);
    }
}

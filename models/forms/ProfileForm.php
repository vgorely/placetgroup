<?php

namespace app\models\forms;

use Yii;
use app\models\User;

class ProfileForm extends \app\modules\core\base\Model
{
    public $login;
    public $first_name;
    public $last_name;
    public $email;
    public $phone;
    public $birth_date;

    /* @var User */
    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $uniqueFilter = function($query) { $query->andWhere(['not', ['user_id' => $this->user->user_id]]); };
        
        return [
            [['login', 'first_name', 'last_name'], 'required'],
            [['login'], 'string', 'min' => 3, 'max' => 32],
            [['login'], 'match', 'pattern' => '/^[a-z]*$/i', 'message' => Yii::t('validation', 'lettersOnly')],
            [['login'], 'unique', 'targetClass' => 'app\models\User', 'filter' => $uniqueFilter],
            [['first_name', 'last_name'], 'string', 'min' => 2, 'max' => 64],
            [['first_name', 'last_name'], 'match', 'pattern' => '/^[a-z]*$/i', 'message' => Yii::t('validation', 'lettersOnly')],
            [['email'], 'string', 'max' => 128],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => 'app\models\User', 'filter' => $uniqueFilter],
            [['phone'], 'string', 'max' => 32],
            [['phone'], 'match', 'pattern' => '/^[0-9]*$/', 'message' => Yii::t('validation', 'numbersOnly')],
            [['birth_date'], 'string'],
            [['birth_date'], 'date', 'format' => Yii::$app->params['format.db.date']],
        ];
    }
    
    /**
     * @param User $user
     */
    public function setUser($user)
    {
        if ($user instanceof User) {
            $this->login = $user->login;
            $this->first_name = $user->first_name;
            $this->last_name = $user->last_name;
            $this->email = $user->email;
            $this->phone = $user->phone;
            $this->birth_date = $user->birth_date;

            $this->user = $user;
        }
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if ($this->user instanceof User) {
            $this->user->login = $this->login;
            $this->user->first_name = $this->first_name;
            $this->user->last_name = $this->last_name;
            $this->user->email = $this->email;
            $this->user->phone = $this->phone;
            $this->user->birth_date = $this->birth_date;
        }
        
        return $this->user;
    }

    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->getUser();
        return $user->extendedSave();
    }
}

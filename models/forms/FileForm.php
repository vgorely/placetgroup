<?php

namespace app\models\forms;

use Yii;
use app\models\File;
use app\models\User;
use yii\web\UploadedFile;

class FileForm extends \app\modules\core\base\Model
{
    public $file;

    /* @var User */
    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file'], 'required'],
            [['file'], 'file', 'maxSize' => 1048576],
        ];
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        if ($user instanceof User) {
            $this->user = $user;
        }
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function save()
    {
        $uploadedFile = UploadedFile::getInstance($this, 'file');
        $this->file = $uploadedFile->name;
        
        if (!$this->validate()) {
            return false;
        }

        $user = $this->getUser();
        $file = new File([
            'user_id' => $user->user_id,
            'path' => File::generatePath($user->user_id, $uploadedFile),
            'created_date' => date('Y-m-d H:i:s')
        ]);
        if (!$file->addToStorage($uploadedFile)) {
            return false;
        }
        return $file->save();
    }
}

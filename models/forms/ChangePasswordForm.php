<?php

namespace app\models\forms;

use Yii;
use app\models\User;

class ChangePasswordForm extends \app\modules\core\base\Model
{
    public $old_password;
    public $new_password;
    public $repeat_new_password;

    /* @var User */
    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [];
        $rules[] = [['old_password', 'new_password', 'repeat_new_password'], 'required'];
        $rules[] = [['new_password'], 'string', 'min' => 6];
        $rules[] = [['repeat_new_password'], 'compare', 'compareAttribute' => 'new_password', 'operator' => '=='];
        $rules[] = [['old_password'], 'validateOldPassword'];
        return $rules;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        if ($user instanceof User) {
            $this->user = $user;
        }
    }

    /**
     * @return User
     */
    public function getUser()
    {
        if ($this->user instanceof User) {
            $this->user->setPassword($this->new_password);
        }
        return $this->user;
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function validateOldPassword()
    {
        if (!$this->user->validatePassword($this->old_password)) {
            $this->addError('old_password', 'Old password incorrect.');
        }
    }
    
    /**
     * @return bool
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->getUser();
        return $user->extendedSave();
    }
}

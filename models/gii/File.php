<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property integer $file_id
 * @property integer $user_id
 * @property string $path
 * @property integer $active
 * @property string $created_date
 *
 * @property User $user
 */
class File extends \app\modules\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'path', 'created_date'], 'required'],
            [['user_id', 'active'], 'integer'],
            [['created_date'], 'safe'],
            [['path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file_id' => 'File ID',
            'user_id' => 'User ID',
            'path' => 'Path',
            'active' => 'Active',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['user_id' => 'user_id']);
    }
}

<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $user_id
 * @property string $login
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property string $birth_date
 * @property string $created_date
 *
 * @property File[] $files
 * @property UserHistory[] $userHistories
 */
class User extends \app\modules\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'first_name', 'last_name', 'created_date'], 'required'],
            [['birth_date', 'created_date'], 'safe'],
            [['login', 'phone'], 'string', 'max' => 32],
            [['password', 'email'], 'string', 'max' => 128],
            [['first_name', 'last_name'], 'string', 'max' => 64],
            [['login'], 'unique'],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'login' => 'Login',
            'password' => 'Password',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'birth_date' => 'Birth Date',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(\app\models\File::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHistories()
    {
        return $this->hasMany(\app\models\UserHistory::className(), ['user_id' => 'user_id']);
    }
}

<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "user_history".
 *
 * @property integer $user_history_id
 * @property integer $user_id
 * @property string $before
 * @property string $after
 * @property string $created_date
 *
 * @property User $user
 */
class UserHistory extends \app\modules\core\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_date'], 'required'],
            [['user_id'], 'integer'],
            [['before', 'after'], 'string'],
            [['created_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_history_id' => 'User History ID',
            'user_id' => 'User ID',
            'before' => 'Before',
            'after' => 'After',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['user_id' => 'user_id']);
    }
}

<?php

namespace app\models;

use Yii;
use app\modules\core\helpers\FileHelper;

class File extends gii\File
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['created_date'], 'date', 'format' => Yii::$app->params['format.db.datetime']];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if ($this->active !== null) {
            $this->active = intval($this->active);
        }
        
        return parent::beforeValidate();
    }

    /**
     * @return bool|string
     */
    public function getAbsolutePath()
    {
        return Yii::getAlias('@uploads' . $this->path);
    }
    
    /**
     * @param int $userId
     * @param string $filename
     * @return string
     */
    public static function generatePath($userId, $filename)
    {
        $path = '/users/' . $userId . '/files/' . $filename;
        
        $file = new File(['path' => $path]);
        while (is_file($file->getAbsolutePath())) {
            $file->path = FileHelper::incrementFilename($file->path);
        }
        
        return $file->path;
    }
    
    /**
     * @param \yii\web\UploadedFile $uploadedFile 
     * @return bool
     */
    public function addToStorage($uploadedFile)
    {
        $absolutePath = $this->getAbsolutePath($this->path);
        FileHelper::createDirectory(dirname($absolutePath), 0777);
        return $uploadedFile->saveAs($absolutePath);
    }

    /**
     * @return bool
     */
    public function deleteFromStorage()
    {
        $absolutePath = $this->getAbsolutePath($this->path);
        if (!is_file($absolutePath)) {
            return false;
        }
        return unlink($absolutePath);
    }
}

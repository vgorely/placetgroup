<?php

namespace app\models\search;

use Yii;
use app\models\File;
use app\models\User;
use yii\data\ActiveDataProvider;

class FileSearch extends \yii\base\Model
{
    public $filename;
    public $created_date;

    /* @var User */
    private $user;

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        if ($user instanceof User) {
            $this->user = $user;
        }
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = File::find()->where(['user_id' => $this->user->user_id, 'active' => true]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_date' => SORT_DESC]],
            'pagination' => ['pageSize' => Yii::$app->params['grid.pageSize']],
        ]);

        return $dataProvider;
    }
}

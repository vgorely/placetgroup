<?php

namespace app\models;

use Yii;

class UserHistory extends gii\UserHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['created_date'], 'date', 'format' => Yii::$app->params['format.db.datetime']];
        return $rules;
    }
}

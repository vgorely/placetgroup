function afterFileDelete(response) {
    if (_.isObject(response) && _.has(response, 'flash')) {
        var flash = response.flash;
        app.flash(flash.message, flash.type);
    }
    $.pjax.reload('#' + fileList);
}

$('body')
    .on('click', '.add-file', function() {
        var self = $(this),
            $modal = $('#' + fileModal).data('bs.modal');

        $.get(self.is('a') ? self.attr('href') : self.data('href'), function(response) {
            $modal.$element.find('.modal-header span').html(response.header);
            $modal.$element.find('.modal-body').html(response.body);
            $modal.show();
        });

        return false;
    })
    .on('click', '.delete-file', function() {
        var self = $(this);

        if (!app.confirm('Are you sure you want to delete this file?', function() {
            $.ajax({
                url: self.attr('href'),
                success: afterFileDelete
            });
        }));

        return false;
    });

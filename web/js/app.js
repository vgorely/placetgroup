var app = {
    confirm: function(message, ok, cancel) {
        ok = _.isFunction(ok) ? ok : function() {};
        cancel = _.isFunction(cancel) ? cancel : function() {};

        bootbox.confirm(message, function(confirm) {
            confirm ? ok.call() : cancel.call();
        });
    },
    flash: function(message, type) {
        if (_.isUndefined(type)) {
            type = 'info';
        }

        notif({
            msg: message,
            position: 'right',
            type: type,
            autohide: type == 'error' ? false : true,
            width: (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth) / 3,
            multiline: true
        });
    }
};

$(document)
    .ajaxError(function(event, xhr, options) {
        try {
            var message = xhr.responseText;
            if (message) {
                app.flash(message, 'error');
            }
        } catch(e) {
        }
    });

// 'beforeSubmit' function for active form
function beforeSubmit($form) {
    var data = $form.data('yiiActiveForm');

    $form.find('[type="submit"]').attr('disabled', 'disabled');
    $.post($form.attr('action'), $form.serialize())
        .success(function(response) {
            if (!_.isObject(response)) {
                return;
            }

            if (_.has(response, 'success')) {
                var $modal = $form.closest('.modal'),
                    success = $form.data('success');

                if (_.isFunction(window[success])) {
                    window[success]($form, response);
                }
                if ($modal.length) {
                    $modal.modal('hide');
                }
            }
            if (_.has(response, 'errors')) {
                var errors = response.errors;
                for (var i in data.attributes) {
                    var attribute = data.attributes[i],
                        $container = $form.find(attribute.container),
                        $error = $container.find(attribute.error),
                        hasError = errors && _.isArray(errors[attribute.name]) && errors[attribute.name].length;

                    if (hasError) {
                        $error.text(errors[attribute.name][0]);
                        $container.removeClass(data.settings.validatingCssClass + ' ' + data.settings.successCssClass)
                            .addClass(data.settings.errorCssClass);
                    } else {
                        $error.text('');
                        $container.removeClass(data.settings.validatingCssClass + ' ' + data.settings.errorCssClass + ' ')
                            .addClass(data.settings.successCssClass);
                    }
                }
            }
            if (_.has(response, 'flash')) {
                var flash = response.flash;
                app.flash(flash.message, flash.type);
            }
        })
        .complete(function() {
            $form.find('[type="submit"]').removeAttr('disabled');
        });
    return false;
}

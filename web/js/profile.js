function afterProfileSave() {
    $.pjax.reload('#' + profile);
}

$('body')
    .on('click', '.edit-profile', function() {
        var self = $(this),
            $modal = $('#' + profileModal).data('bs.modal');

        $.get(self.is('a') ? self.attr('href') : self.data('href'), function(response) {
            $modal.$element.find('.modal-header span').html(response.header);
            $modal.$element.find('.modal-body').html(response.body);
            $modal.show();
        });

        return false;
    });
